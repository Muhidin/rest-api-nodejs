'use strict';
module.exports = function(app) {

// multer to handling file
const multer = require('multer');
const upload = multer({dest: __dirname + '/public/images'});

var brgList = require('../controllers/appController');
var custList = require('../controllers/custController');
var config = require('../controllers/configTokoController');
var users = require('../controllers/usersController');
var image = require('../controllers/uploadImage');

var middleware = require('../middleware/users.js');

var item = require('../controllers/list-app/items');

const main_view = require('../controllers/main_view');

const valdoexport = require('../controllers/valdoexport');
const menu = require('../controllers/menu/Menu');
const user = require('../controllers/auth/register.controller');
const login = require('../controllers/auth/login.controller');

// product
const product = require('../controllers/master/product.controller');

const authenticateToken = require('../middleware/authorize')

	// const bcrypt = require('bcryptjs');
	// const uuid = require('uuid');
	// const jwt = require('jsonwebtoken');

	/* handle views */
	app.route('/')
		.get(main_view.homepage);

	app.route('/about')
		.get(main_view.aboutpage);

	app.route('/barang_view')
		.get(main_view.listBarang);

	app.route('/barang_view/edit/:id')
		.get(main_view.getBarangById);
		//.post(main_view.updateBarang);

	/* end handle views */

	// userList Routes
	app.route('/barang')
		.post(brgList.createBarang);

	app.route('/barang')
		.get(authenticateToken,brgList.list_barang)

	app.route('/barang/upload')
		.post(brgList.uploadBarang);

	
	 app.route('/barang/:id')
	 	.get(brgList.getBrgById)
	 	.put(brgList.updateBarang)
	 	.delete(brgList.deleteBarang);

	 app.route('/customer')
	 	.get(custList.list_customer)
	 	.post(custList.createCust);

	 app.route('/customer/:id')
	 	.get(custList.getCustById)
	 	.put(custList.updateCustomer)
		.delete(custList.deleteCustomer);
	
	app.route('/config')
		.post(config.createToko);

	app.route('/upload')
		.post(image.uploadFiles);

	app.route('/item')
		.get(item.listItems);

	app.route('/upload/valdo')
		.post(valdoexport.uploadData);

	app.route('/upload/auto_upload')
		.get(valdoexport.getAll);

	// register user
	app.route('/auth/register')
		.post(authenticateToken,user.registerUser);

		// register user
	app.route('/auth/login')
		.post(login.login);

	app.route('/menu')
		.post(upload.single('images'),menu.saveMenu);


	// route product
	app.route('/product')
		.post(authenticateToken,product.addProduct)
		.get(authenticateToken,product.getAllProduct)
	app.route('/product/update/:id')
		.post(authenticateToken,product.updateProduct)
	app.route('/product/delete/:id')
		.post(authenticateToken,product.deleteProduct)




};