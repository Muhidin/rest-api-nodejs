const express = require('express');
const app = express();
let bodyParser = require('body-parser');
const mysql = require('mysql');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

//default route
app.get('/', (req, res) => {
	return res.send({ error: true, message: 'hello' })
});

// DB connection
const dbConn = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database:'testing'
});

// connect to database
dbConn.connect();

//Retrieve all users
app.get('/users', (req, res) => {
	dbConn.query('SELECT * FROM users', (error, results, fields) => {
		if (error) throw error;
		return res.send({ error: false, data: results, message: 'users list'});
	});
});

//Retrieve user with id
app.get('/users/:id', (req, res) => {
	let user_id = req.params.id;

	if (!user_id) {
		return res.status(400).send({ error: true, message: 'Please provide user id'});
	}

	dbConn.query('SELECT * FROM users where id=?', user_id, (error, results, fields) => {
		if (error) throw error;
		return res.send({ error: false, data: results[0], message: 'users list' });
	});
});


// add new user
app.post('/users', (req, res) => {

	let user = req.body.user;


	if (!user) {
		return res.status(400).send({ error:true, message: 'Please provide user' });
	}

	dbConn.query("INSERT INTO users SET ? ", { user: user }, (error, results, fields) => {
		if (error) throw error;

		return res.send({ error: false, data: results, message: 'New user has been created' });
	});
});


// update user with id
app.put('/users', function (req, res) {
	let user_id = req.body.user_id;
	let user = req.body.user;

	if (!user_id || !user) {
		return res.status(400).send({ error: user, message:'Please provide user and id user' });
	}

	dbConn.query("UPDATE users SET user = ? WHERE id = ?", [user, user_id], function (error, results, fields) {
		if (error) throw error ;

		return res.send({ error: false, data: results, message:'user has been update succesfully'});
	});
});



// set port

app.listen(8000, () => {
	console.log('App is running on port 8000');
});

module.exports = app;