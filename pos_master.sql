-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: pos_master
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `det_belanja_bulanan`
--

DROP TABLE IF EXISTS `det_belanja_bulanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_belanja_bulanan` (
  `no_invoice` varchar(20) NOT NULL DEFAULT '',
  `kode_brg` varchar(10) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`no_invoice`,`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_belanja_bulanan`
--

LOCK TABLES `det_belanja_bulanan` WRITE;
/*!40000 ALTER TABLE `det_belanja_bulanan` DISABLE KEYS */;
INSERT INTO `det_belanja_bulanan` VALUES ('INV-20200707001','ITM-002',100,120),('INV-20200707002','ITM-002',1000,120),('INV-20200707002','ITM-003',1000,120),('INV-20200707002','ITM-004',1000,120);
/*!40000 ALTER TABLE `det_belanja_bulanan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `det_belanja_bef_in` BEFORE INSERT ON `det_belanja_bulanan` FOR EACH ROW BEGIN
	
	INSERT INTO kartu_stok (tgl,in_,last_stok,barang,ket) 
	VALUES(CURDATE(),new.qty,new.qty +(SELECT stok FROM m_brg WHERE kode_brg = new.kode_brg),new.kode_brg,'BELANJA BULANAN');
	update m_brg set stok = stok + new.qty where kode_brg = new.kode_brg;
	
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `kartu_stok`
--

DROP TABLE IF EXISTS `kartu_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kartu_stok` (
  `no_ref` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `in_` int(5) DEFAULT NULL,
  `out_` int(5) DEFAULT NULL,
  `last_stok` int(5) DEFAULT NULL,
  `barang` varchar(10) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`no_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kartu_stok`
--

LOCK TABLES `kartu_stok` WRITE;
/*!40000 ALTER TABLE `kartu_stok` DISABLE KEYS */;
INSERT INTO `kartu_stok` VALUES (1,'2020-07-07',1000,0,1000,'ITM-001','STOCK AWAL'),(2,'2020-07-07',1000,0,1000,'ITM-002','STOCK AWAL'),(3,'2020-07-07',1000,0,1000,'ITM-003','STOCK AWAL'),(4,'2020-07-07',1000,0,1000,'ITM-004','STOCK AWAL'),(5,'2020-07-07',1000,0,1000,'ITM-005','STOCK AWAL'),(6,'2020-07-07',1000,0,1000,'ITM-006','STOCK AWAL'),(7,'2020-07-07',1000,0,1000,'ITM-007','STOCK AWAL'),(8,'2020-07-07',1000,0,1000,'ITM-008','STOCK AWAL'),(9,'2020-07-07',1000,0,1000,'ITM-009','STOCK AWAL'),(10,'2020-07-07',1000,0,1000,'ITM-010','STOCK AWAL'),(11,'2020-07-07',1000,0,1000,'ITM-011','STOCK AWAL'),(12,'2020-07-07',1000,0,1000,'ITM-012','STOCK AWAL'),(13,'2020-07-07',1000,0,1000,'ITM-013','STOCK AWAL'),(14,'2020-07-07',1000,0,1000,'ITM-014','STOCK AWAL'),(15,'2020-07-07',1000,0,1000,'ITM-015','STOCK AWAL'),(16,'2020-07-07',1000,0,1000,'ITM-016','STOCK AWAL'),(17,'2020-07-07',1000,0,1000,'ITM-017','STOCK AWAL'),(18,'2020-07-07',1000,0,1000,'ITM-018','STOCK AWAL'),(19,'2020-07-07',1000,0,1000,'ITM-019','STOCK AWAL'),(20,'2020-07-07',1000,0,1000,'ITM-020','STOCK AWAL'),(21,'2020-07-07',1000,0,1000,'ITM-021','STOCK AWAL'),(22,'2020-07-07',1000,0,1000,'ITM-022','STOCK AWAL'),(23,'2020-07-07',1000,0,1000,'ITM-023','STOCK AWAL'),(24,'2020-07-07',1000,0,1000,'ITM-024','STOCK AWAL'),(25,'2020-07-07',1000,0,1000,'ITM-025','STOCK AWAL'),(26,'2020-07-07',1000,0,1000,'ITM-026','STOCK AWAL'),(27,'2020-07-07',1000,0,1000,'ITM-027','STOCK AWAL'),(28,'2020-07-07',1000,0,1000,'ITM-028','STOCK AWAL'),(29,'2020-07-07',1000,0,1000,'ITM-029','STOCK AWAL'),(30,'2020-07-07',1000,0,1000,'ITM-030','STOCK AWAL'),(31,'2020-07-07',1000,0,1000,'ITM-031','STOCK AWAL'),(32,'2020-07-07',1000,0,1000,'ITM-032','STOCK AWAL'),(33,'2020-07-07',1000,0,1000,'ITM-033','STOCK AWAL'),(34,'2020-07-07',1000,0,1000,'ITM-034','STOCK AWAL'),(35,'2020-07-07',1000,0,1000,'ITM-035','STOCK AWAL'),(36,'2020-07-07',1000,0,1000,'ITM-036','STOCK AWAL'),(37,'2020-07-07',1000,0,1000,'ITM-037','STOCK AWAL'),(38,'2020-07-07',1000,0,1000,'ITM-038','STOCK AWAL'),(39,'2020-07-07',1000,0,1000,'ITM-039','STOCK AWAL'),(40,'2020-07-07',1000,0,1000,'ITM-040','STOCK AWAL'),(41,'2020-07-07',1000,0,1000,'ITM-041','STOCK AWAL'),(42,'2020-07-07',1000,0,1000,'ITM-042','STOCK AWAL'),(43,'2020-07-07',1000,0,1000,'ITM-043','STOCK AWAL'),(44,'2020-07-07',1000,0,1000,'ITM-044','STOCK AWAL'),(45,'2020-07-07',1000,0,1000,'ITM-045','STOCK AWAL'),(46,'2020-07-07',1000,0,1000,'ITM-046','STOCK AWAL'),(47,'2020-07-07',1000,0,1000,'ITM-047','STOCK AWAL'),(48,'2020-07-07',1000,0,1000,'ITM-048','STOCK AWAL'),(49,'2020-07-07',1000,0,1000,'ITM-049','STOCK AWAL'),(50,'2020-07-07',1000,0,1000,'ITM-050','STOCK AWAL'),(51,'2020-07-07',1000,0,1000,'ITM-051','STOCK AWAL'),(52,'2020-07-07',1000,0,1000,'ITM-052','STOCK AWAL'),(53,'2020-07-07',1000,0,1000,'ITM-053','STOCK AWAL'),(54,'2020-07-07',1000,0,1000,'ITM-054','STOCK AWAL'),(55,'2020-07-07',1000,0,1000,'ITM-055','STOCK AWAL'),(56,'2020-07-07',1000,0,1000,'ITM-056','STOCK AWAL'),(57,'2020-07-07',1000,0,1000,'ITM-057','STOCK AWAL'),(58,'2020-07-07',1000,0,1000,'ITM-058','STOCK AWAL'),(59,'2020-07-07',1000,0,1000,'ITM-059','STOCK AWAL'),(60,'2020-07-07',1000,0,1000,'ITM-060','STOCK AWAL'),(61,'2020-07-07',1000,0,1000,'ITM-061','STOCK AWAL'),(62,'2020-07-07',1000,0,1000,'ITM-062','STOCK AWAL'),(63,'2020-07-07',1000,0,1000,'ITM-063','STOCK AWAL'),(64,'2020-07-07',1000,0,1000,'ITM-064','STOCK AWAL'),(65,'2020-07-07',1000,0,1000,'ITM-065','STOCK AWAL'),(66,'2020-07-07',1000,0,1000,'ITM-066','STOCK AWAL'),(67,'2020-07-07',1000,0,1000,'ITM-067','STOCK AWAL'),(68,'2020-07-07',1000,0,1000,'ITM-068','STOCK AWAL'),(69,'2020-07-07',1000,0,1000,'ITM-069','STOCK AWAL'),(70,'2020-07-07',1000,0,1000,'ITM-070','STOCK AWAL'),(71,'2020-07-07',1000,0,1000,'ITM-071','STOCK AWAL'),(72,'2020-07-07',1000,0,1000,'ITM-072','STOCK AWAL'),(73,'2020-07-07',1000,0,1000,'ITM-073','STOCK AWAL'),(74,'2020-07-07',1000,0,1000,'ITM-074','STOCK AWAL'),(75,'2020-07-07',1000,0,1000,'ITM-075','STOCK AWAL'),(76,'2020-07-07',1000,0,1000,'ITM-076','STOCK AWAL'),(77,'2020-07-07',1000,0,1000,'ITM-077','STOCK AWAL'),(78,'2020-07-07',1000,0,1000,'ITM-078','STOCK AWAL'),(79,'2020-07-07',1000,0,1000,'ITM-079','STOCK AWAL'),(80,'2020-07-07',1000,0,1000,'ITM-080','STOCK AWAL'),(81,'2020-07-07',1000,0,1000,'ITM-081','STOCK AWAL'),(82,'2020-07-07',1000,0,1000,'ITM-082','STOCK AWAL'),(83,'2020-07-07',1000,0,1000,'ITM-083','STOCK AWAL'),(84,'2020-07-07',1000,0,1000,'ITM-084','STOCK AWAL'),(85,'2020-07-07',1000,0,1000,'ITM-085','STOCK AWAL'),(86,'2020-07-07',1000,0,1000,'ITM-086','STOCK AWAL'),(87,'2020-07-07',1000,0,1000,'ITM-087','STOCK AWAL'),(88,'2020-07-07',1000,0,1000,'ITM-088','STOCK AWAL'),(89,'2020-07-07',1000,0,1000,'ITM-089','STOCK AWAL'),(90,'2020-07-07',1000,0,1000,'ITM-090','STOCK AWAL'),(91,'2020-07-07',1000,0,1000,'ITM-091','STOCK AWAL'),(92,'2020-07-07',1000,0,1000,'ITM-092','STOCK AWAL'),(93,'2020-07-07',1000,0,1000,'ITM-093','STOCK AWAL'),(94,'2020-07-07',1000,0,1000,'ITM-094','STOCK AWAL'),(95,'2020-07-07',1000,0,1000,'ITM-095','STOCK AWAL'),(96,'2020-07-07',1000,0,1000,'ITM-096','STOCK AWAL'),(97,'2020-07-07',1000,0,1000,'ITM-097','STOCK AWAL'),(98,'2020-07-07',1000,0,1000,'ITM-098','STOCK AWAL'),(99,'2020-07-07',1000,0,1000,'ITM-099','STOCK AWAL'),(100,'2020-07-07',1000,0,1000,'ITM-100','STOCK AWAL'),(101,'2020-07-07',1000,0,1000,'ITM-101','STOCK AWAL'),(102,'2020-07-07',1000,0,1000,'ITM-102','STOCK AWAL'),(103,'2020-07-07',1000,0,1000,'ITM-103','STOCK AWAL'),(104,'2020-07-07',1000,0,1000,'ITM-104','STOCK AWAL'),(105,'2020-07-07',1000,0,1000,'ITM-105','STOCK AWAL'),(106,'2020-07-07',1000,0,1000,'ITM-106','STOCK AWAL'),(107,'2020-07-07',1000,0,1000,'ITM-107','STOCK AWAL'),(108,'2020-07-07',1000,0,1000,'ITM-108','STOCK AWAL'),(109,'2020-07-07',1000,0,1000,'ITM-109','STOCK AWAL'),(110,'2020-07-07',1000,0,1000,'ITM-110','STOCK AWAL'),(111,'2020-07-07',1000,0,1000,'ITM-111','STOCK AWAL'),(112,'2020-07-07',100,NULL,1100,'ITM-001','BELANJA TUNAI'),(113,'2020-07-07',100,NULL,1100,'ITM-002','BELANJA BULANAN'),(114,'2020-07-07',NULL,100,900,'ITM-008','SALES'),(115,'2020-07-07',NULL,100,900,'ITM-009','SALES'),(116,'2020-07-07',NULL,100,900,'ITM-010','SALES'),(117,'2020-07-07',1000,NULL,2100,'ITM-002','BELANJA BULANAN'),(118,'2020-07-07',1000,NULL,2000,'ITM-003','BELANJA BULANAN'),(119,'2020-07-07',1000,NULL,2000,'ITM-004','BELANJA BULANAN'),(120,'2020-07-07',NULL,100,1000,'ITM-001','SALES'),(121,'2020-07-07',NULL,100,2000,'ITM-002','SALES'),(122,'2020-07-07',NULL,100,1900,'ITM-003','SALES'),(123,'2020-07-08',NULL,100,900,'ITM-001','SALES'),(124,'2020-07-08',NULL,100,1900,'ITM-002','SALES'),(125,'2020-07-08',NULL,100,1800,'ITM-003','SALES'),(126,'2020-07-08',NULL,100,800,'ITM-008','SALES'),(127,'2020-07-08',NULL,100,800,'ITM-009','SALES'),(128,'2020-07-08',NULL,100,800,'ITM-010','SALES'),(129,'2020-07-08',NULL,100,1900,'ITM-004','SALES'),(130,'2020-07-08',NULL,100,900,'ITM-005','SALES'),(131,'2020-07-08',NULL,100,900,'ITM-006','SALES'),(132,'2020-07-08',NULL,100,800,'ITM-001','SALES'),(133,'2020-07-08',NULL,100,1800,'ITM-002','SALES'),(134,'2020-07-08',NULL,100,1700,'ITM-003','SALES'),(135,'2020-07-08',NULL,100,800,'ITM-001','SALES'),(136,'2020-07-08',NULL,100,1800,'ITM-002','SALES'),(137,'2020-07-08',NULL,100,1700,'ITM-003','SALES'),(138,'2020-07-08',NULL,100,700,'ITM-008','SALES'),(139,'2020-07-08',NULL,100,700,'ITM-009','SALES'),(140,'2020-07-08',NULL,100,700,'ITM-010','SALES'),(141,'2020-07-08',NULL,100,1800,'ITM-004','SALES'),(142,'2020-07-08',NULL,100,800,'ITM-005','SALES'),(143,'2020-07-08',NULL,100,800,'ITM-006','SALES'),(144,'2020-07-08',NULL,100,1700,'ITM-004','SALES'),(145,'2020-07-08',NULL,100,700,'ITM-005','SALES'),(146,'2020-07-08',NULL,100,700,'ITM-006','SALES'),(147,'2020-07-08',NULL,100,1600,'ITM-004','SALES'),(148,'2020-07-08',NULL,100,600,'ITM-005','SALES'),(149,'2020-07-08',NULL,100,600,'ITM-006','SALES'),(150,'2020-07-08',NULL,100,700,'ITM-001','SALES'),(151,'2020-07-08',NULL,100,1700,'ITM-002','SALES'),(152,'2020-07-08',NULL,100,1600,'ITM-003','SALES'),(153,'2020-07-08',NULL,100,600,'ITM-001','SALES'),(154,'2020-07-08',NULL,100,1600,'ITM-002','SALES'),(155,'2020-07-08',NULL,100,1500,'ITM-003','SALES'),(156,'2020-07-08',NULL,100,600,'ITM-008','SALES'),(157,'2020-07-08',NULL,100,600,'ITM-009','SALES'),(158,'2020-07-08',NULL,100,600,'ITM-010','SALES'),(159,'2020-07-08',NULL,100,500,'ITM-008','SALES'),(160,'2020-07-08',NULL,100,500,'ITM-009','SALES'),(161,'2020-07-08',NULL,100,500,'ITM-010','SALES'),(162,'2020-07-08',NULL,100,1500,'ITM-004','SALES'),(163,'2020-07-08',NULL,100,500,'ITM-005','SALES'),(164,'2020-07-08',NULL,100,500,'ITM-006','SALES'),(165,'2020-07-08',NULL,100,400,'ITM-008','SALES'),(166,'2020-07-08',NULL,100,400,'ITM-009','SALES'),(167,'2020-07-08',NULL,100,400,'ITM-010','SALES'),(168,'2020-10-11',NULL,100,1400,'ITM-004','SALES'),(169,'2020-10-11',NULL,100,400,'ITM-005','SALES'),(170,'2020-10-11',NULL,100,400,'ITM-006','SALES'),(171,'2020-10-11',NULL,100,1300,'ITM-004','SALES'),(172,'2020-10-11',NULL,100,300,'ITM-005','SALES'),(173,'2020-10-11',NULL,100,300,'ITM-006','SALES'),(174,'2021-01-21',1000,0,1000,'ITM-112','STOCK AWAL'),(175,'2021-01-21',1000,0,1000,'ITM-113','STOCK AWAL'),(176,'2021-01-21',1000,0,1000,'ITM-114','STOCK AWAL'),(177,'2021-01-21',1000,0,1000,'ITM-115','STOCK AWAL'),(178,'2021-01-21',1000,0,1000,'ITM-116','STOCK AWAL'),(179,'2021-01-21',1000,0,1000,'ITM-117','STOCK AWAL'),(180,'2021-01-21',1000,0,1000,'ITM-118','STOCK AWAL'),(181,'2021-01-21',1000,0,1000,'ITM-119','STOCK AWAL'),(182,'2021-01-21',1000,0,1000,'ITM-120','STOCK AWAL'),(183,'2021-01-21',1000,0,1000,'ITM-121','STOCK AWAL'),(184,'2021-01-21',1000,0,1000,'ITM-122','STOCK AWAL'),(185,'2021-01-21',1000,0,1000,'ITM-123','STOCK AWAL'),(186,'2021-01-21',1000,0,1000,'ITM-124','STOCK AWAL'),(187,'2021-01-21',1000,0,1000,'ITM-125','STOCK AWAL'),(188,'2021-01-21',1000,0,1000,'ITM-126','STOCK AWAL'),(189,'2021-01-21',1000,0,1000,'ITM-127','STOCK AWAL'),(190,'2021-01-21',1000,0,1000,'ITM-128','STOCK AWAL'),(191,'2021-01-21',1000,0,1000,'ITM-129','STOCK AWAL'),(192,'2021-01-21',1000,0,1000,'ITM-130','STOCK AWAL'),(193,'2021-01-21',1000,0,1000,'ITM-131','STOCK AWAL'),(194,'2021-01-21',1000,0,1000,'ITM-132','STOCK AWAL'),(195,'2021-01-21',1000,0,1000,'ITM-133','STOCK AWAL'),(196,'2021-01-21',1000,0,1000,'ITM-134','STOCK AWAL'),(197,'2021-01-21',1000,0,1000,'ITM-135','STOCK AWAL'),(198,'2021-01-21',1000,0,1000,'ITM-136','STOCK AWAL'),(199,'2021-01-21',1000,0,1000,'ITM-137','STOCK AWAL'),(200,'2021-01-21',1000,0,1000,'ITM-138','STOCK AWAL'),(201,'2021-01-21',1000,0,1000,'ITM-139','STOCK AWAL'),(202,'2021-01-21',1000,0,1000,'ITM-140','STOCK AWAL'),(203,'2021-01-21',1000,0,1000,'ITM-112','STOCK AWAL'),(204,'2021-01-21',1000,0,1000,'ITM-113','STOCK AWAL'),(205,'2021-01-21',1000,0,1000,'ITM-114','STOCK AWAL'),(206,'2021-01-21',1000,0,1000,'ITM-115','STOCK AWAL'),(207,'2021-01-21',1000,0,1000,'ITM-116','STOCK AWAL'),(208,'2021-01-21',1000,0,1000,'ITM-117','STOCK AWAL'),(209,'2021-01-21',1000,0,1000,'ITM-118','STOCK AWAL'),(210,'2021-01-21',1000,0,1000,'ITM-119','STOCK AWAL'),(211,'2021-01-21',1000,0,1000,'ITM-120','STOCK AWAL'),(212,'2021-01-21',1000,0,1000,'ITM-121','STOCK AWAL'),(213,'2021-01-21',1000,0,1000,'ITM-122','STOCK AWAL'),(214,'2021-01-21',1000,0,1000,'ITM-123','STOCK AWAL'),(215,'2021-01-21',1000,0,1000,'ITM-124','STOCK AWAL'),(216,'2021-01-21',1000,0,1000,'ITM-125','STOCK AWAL'),(217,'2021-01-21',1000,0,1000,'ITM-126','STOCK AWAL'),(218,'2021-01-21',1000,0,1000,'ITM-127','STOCK AWAL'),(219,'2021-01-21',1000,0,1000,'ITM-128','STOCK AWAL'),(220,'2021-01-21',1000,0,1000,'ITM-129','STOCK AWAL'),(221,'2021-01-21',1000,0,1000,'ITM-130','STOCK AWAL'),(222,'2021-01-21',1000,0,1000,'ITM-131','STOCK AWAL'),(223,'2021-01-21',1000,0,1000,'ITM-132','STOCK AWAL'),(224,'2021-01-21',1000,0,1000,'ITM-133','STOCK AWAL'),(225,'2021-01-21',1000,0,1000,'ITM-134','STOCK AWAL'),(226,'2021-01-21',1000,0,1000,'ITM-135','STOCK AWAL'),(227,'2021-01-21',1000,0,1000,'ITM-136','STOCK AWAL'),(228,'2021-01-21',1000,0,1000,'ITM-137','STOCK AWAL'),(229,'2021-01-21',1000,0,1000,'ITM-138','STOCK AWAL'),(230,'2021-01-21',1000,0,1000,'ITM-139','STOCK AWAL'),(231,'2021-01-21',1000,0,1000,'ITM-140','STOCK AWAL');
/*!40000 ALTER TABLE `kartu_stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `kategori` varchar(15) NOT NULL,
  PRIMARY KEY (`kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES ('COKELAT'),('GULA'),('KOPI');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_brg`
--

DROP TABLE IF EXISTS `m_brg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_brg` (
  `kode_brg` varchar(10) NOT NULL,
  `nama_brg` varchar(35) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `stok` int(5) DEFAULT NULL,
  `unit` varchar(15) DEFAULT NULL,
  `kategori` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_brg`
--

LOCK TABLES `m_brg` WRITE;
/*!40000 ALTER TABLE `m_brg` DISABLE KEYS */;
INSERT INTO `m_brg` VALUES ('ITM-112','ABC112',100,1000,'gr','KOPI'),('ITM-113','ABC113',100,1000,'gr','KOPI'),('ITM-114','ABC114',100,1000,'gr','KOPI'),('ITM-115','ABC115',100,1000,'gr','KOPI'),('ITM-116','ABC116',100,1000,'gr','KOPI'),('ITM-117','ABC117',100,1000,'gr','KOPI'),('ITM-118','ABC118',100,1000,'gr','KOPI'),('ITM-119','ABC119',100,1000,'gr','KOPI'),('ITM-120','ABC120',100,1000,'gr','KOPI'),('ITM-121','ABC121',100,1000,'gr','KOPI'),('ITM-122','ABC122',100,1000,'gr','KOPI'),('ITM-123','ABC123',100,1000,'gr','KOPI'),('ITM-124','ABC124',100,1000,'gr','KOPI'),('ITM-125','ABC125',100,1000,'gr','KOPI'),('ITM-126','ABC126',100,1000,'gr','KOPI'),('ITM-127','ABC127',100,1000,'gr','KOPI'),('ITM-128','ABC128',100,1000,'gr','KOPI'),('ITM-129','ABC129',100,1000,'gr','KOPI'),('ITM-130','ABC130',100,1000,'gr','KOPI'),('ITM-131','ABC131',100,1000,'gr','KOPI'),('ITM-132','ABC132',100,1000,'gr','KOPI'),('ITM-133','ABC133',100,1000,'gr','KOPI'),('ITM-134','ABC134',100,1000,'gr','KOPI'),('ITM-135','ABC135',100,1000,'gr','KOPI'),('ITM-136','ABC136',100,1000,'gr','KOPI'),('ITM-137','ABC137',100,1000,'gr','KOPI'),('ITM-138','ABC138',100,1000,'gr','KOPI'),('ITM-139','ABC139',100,1000,'gr','KOPI'),('ITM-140','ABC140',100,1000,'gr','KOPI');
/*!40000 ALTER TABLE `m_brg` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `m_brg_before_in` BEFORE INSERT ON `m_brg` FOR EACH ROW insert into kartu_stok (tgl,in_,out_,last_stok,barang,ket)
values (curdate(),new.stok,'',new.stok,new.kode_brg,'STOCK AWAL') */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `m_customer`
--

DROP TABLE IF EXISTS `m_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) NOT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_customer`
--

LOCK TABLES `m_customer` WRITE;
/*!40000 ALTER TABLE `m_customer` DISABLE KEYS */;
INSERT INTO `m_customer` VALUES (3,'MUHIDIN','087876695069'),(4,'ARKANANTA WIJAYA','12345'),(5,'PAK RONI','1234');
/*!40000 ALTER TABLE `m_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_kat_menu`
--

DROP TABLE IF EXISTS `m_kat_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_kat_menu` (
  `kat_menu` varchar(15) NOT NULL,
  PRIMARY KEY (`kat_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_kat_menu`
--

LOCK TABLES `m_kat_menu` WRITE;
/*!40000 ALTER TABLE `m_kat_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_kat_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_menu`
--

DROP TABLE IF EXISTS `m_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(35) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `kat_menu` varchar(15) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_menu`
--

LOCK TABLES `m_menu` WRITE;
/*!40000 ALTER TABLE `m_menu` DISABLE KEYS */;
INSERT INTO `m_menu` VALUES (1,'EXPRESSO',15000,'COFFEE','no_image.png'),(2,'BLACK COFFEE',15000,'COFFEE','no_image.png'),(3,'KOPI SUSU DKK',18000,'COFFEE','no_image.png'),(4,'CAPPUCINO',18000,'COFFEE','no_image.png'),(5,'LATTE',18000,'COFFEE','no_image.png'),(6,'MOCCACINO',18000,'COFFEE','no_image.png'),(7,'CARAMEL LATE',18000,'COFFEE','no_image.png'),(8,'VANILA LATTE',18000,'COFFEE','no_image.png'),(9,'HAZELNUT LATTE',18000,'COFFEE','no_image.png'),(10,'RUM LATTE',18000,'COFFEE','no_image.png'),(11,'TARO LATTE',18000,'COFFEE','no_image.png'),(12,'D C RUM LATTE',20000,'COFFEE','no_image.png'),(13,'MATCHA LATTE',20000,'COFFEE','no_image.png'),(14,'BLACK TEA',10000,'NON COFFEE','no_image.png'),(15,'LYCHEE TEA',15000,'NON COFFEE','no_image.png'),(16,'PEACH TEA',15000,'NON COFFEE','no_image.png'),(17,'CHOCOLATE',18000,'NON COFFEE','no_image.png'),(18,'TARO',18000,'NON COFFEE','no_image.png'),(19,'RED VELVET',18000,'NON COFFEE','no_image.png'),(20,'TARO RED',18000,'NON COFFEE','no_image.png'),(21,'MATCHA',18000,'NON COFFEE','no_image.png'),(22,'LEMON TEA',15000,'NON COFFEE','no_image.png'),(23,'CARAMEL',18000,'NON COFFEE','no_image.png'),(24,'VANILLA',18000,'NON COFFEE','no_image.png'),(25,'HAZELNUT',18000,'NON COFFEE','no_image.png'),(26,'CHOCO HAZELNUT',20000,'NON COFFEE','no_image.png'),(27,'CARAMEL',23000,'ICE BLEND','no_image.png'),(28,'VANILLA',23000,'ICE BLEND','no_image.png'),(29,'HAZELNUT',23000,'ICE BLEND','no_image.png'),(30,'RUM',23000,'ICE BLEND','no_image.png'),(31,'CHOCO HAZELNUT',23000,'ICE BLEND','no_image.png'),(32,'CHOCOLATE',23000,'ICE BLEND','no_image.png'),(33,'TARO',23000,'ICE BLEND','no_image.png'),(34,'MATCHA LATTE',23000,'ICE BLEND','no_image.png'),(35,'RED VELVET',23000,'ICE BLEND','no_image.png'),(36,'COOKIES & CREAM',23000,'ICE BLEND','no_image.png'),(37,'TARO RED',23000,'ICE BLEND','no_image.png'),(38,'FRENCH PRESS',15000,'MANUAL BREW','no_image.png'),(39,'V60',15000,'MANUAL BREW','no_image.png'),(40,'JAPANESE ICED',15000,'MANUAL BREW','no_image.png'),(41,'VIETNAM DRIP',15000,'MANUAL BREW','no_image.png'),(42,'COFFEE BEER',20000,'COFFEE BEER','no_image.png'),(43,'NASI GRG DKK',20000,'MAIN COURSE','no_image.png'),(44,'RICE BOWL',20000,'MAIN COURSE','no_image.png'),(45,'SPAGHETTI',18000,'MAIN COURSE','no_image.png'),(46,'NASI GRG PETE',25000,'MAIN COURSE','no_image.png'),(47,'CHICKEN KATSU',30000,'MAIN COURSE','no_image.png'),(48,'IND GORENG',10000,'MIE SERIES','no_image.png'),(49,'IND SOTO',10000,'MIE SERIES','no_image.png'),(50,'IND AYAM BWG',10000,'MIE SERIES','no_image.png'),(51,'IND KARI AYAM',10000,'MIE SERIES','no_image.png'),(52,'ROTI BKR KJU',15000,'ROTI SERIES','no_image.png'),(53,'ROTI BKR CKLT',15000,'ROTI SERIES','no_image.png'),(54,'ROTI BKR KCG',15000,'ROTI SERIES','no_image.png'),(55,'ROTI MZ BEEF',20000,'ROTI SERIES','no_image.png'),(56,'TLR RBS/GRG',5000,'TAMBAHAN MIE','no_image.png'),(57,'KEJU',5000,'TAMBAHAN MIE','no_image.png'),(58,'TLR KRND',8000,'TAMBAHAN MIE','no_image.png'),(59,'TLR KEJU',8000,'TAMBAHAN MIE','no_image.png'),(60,'TLR KEJU KRND',10000,'TAMBAHAN MIE','no_image.png'),(61,'COKELAT',3000,'TAMBAHAN ROTI','no_image.png'),(62,'KEJU',3000,'TAMBAHAN ROTI','no_image.png'),(63,'KACANG',3000,'TAMBAHAN ROTI','no_image.png'),(64,'FRENCH FRIES',10000,'SHARING SNACK','no_image.png'),(65,'OTAK2 + FRIES',15000,'SHARING SNACK','no_image.png'),(66,'NACHO FRIES',20000,'SHARING SNACK','no_image.png'),(67,'ROTI GORENG',20000,'SHARING SNACK','no_image.png'),(68,'TAHU MERCON',15000,'SHARING SNACK','no_image.png'),(69,'RISOL MAYO',15000,'SHARING SNACK','no_image.png');
/*!40000 ALTER TABLE `m_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_resep`
--

DROP TABLE IF EXISTS `m_resep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_resep` (
  `no_resep` varchar(15) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_resep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_resep`
--

LOCK TABLES `m_resep` WRITE;
/*!40000 ALTER TABLE `m_resep` DISABLE KEYS */;
INSERT INTO `m_resep` VALUES ('RSP-001',1),('RSP-002',2),('RSP-003',3);
/*!40000 ALTER TABLE `m_resep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_staf`
--

DROP TABLE IF EXISTS `m_staf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_staf` (
  `id_staf` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  `jabatan` varchar(15) DEFAULT NULL,
  `gaji` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_staf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_staf`
--

LOCK TABLES `m_staf` WRITE;
/*!40000 ALTER TABLE `m_staf` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_staf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_suplier`
--

DROP TABLE IF EXISTS `m_suplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_suplier` (
  `kd_sup` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sup` varchar(35) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  `cp` varchar(25) DEFAULT NULL,
  `no_account` varchar(20) DEFAULT NULL,
  `bank` varchar(15) DEFAULT NULL,
  `an` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`kd_sup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_suplier`
--

LOCK TABLES `m_suplier` WRITE;
/*!40000 ALTER TABLE `m_suplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_suplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_user`
--

DROP TABLE IF EXISTS `m_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` char(1) NOT NULL,
  `id_staf` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `last_logout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) DEFAULT '0',
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_user`
--

LOCK TABLES `m_user` WRITE;
/*!40000 ALTER TABLE `m_user` DISABLE KEYS */;
INSERT INTO `m_user` VALUES (1,'admin','95ce4fa0fae0368871bdd4d363de3dbe83b0c141','1',3,'2021-01-03 13:15:21','0000-00-00 00:00:00',0,NULL),(6,'SEKAR','95ce4fa0fae0368871bdd4d363de3dbe83b0c141','2',NULL,'2021-01-03 13:17:02','0000-00-00 00:00:00',1,NULL),(7,'arkananta','bb9ca1c6c522c25faf4bbe941ebedf2d72ea7048','1',NULL,NULL,'0000-00-00 00:00:00',0,NULL);
/*!40000 ALTER TABLE `m_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_belanja_bulanan`
--

DROP TABLE IF EXISTS `t_belanja_bulanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_belanja_bulanan` (
  `no_invoice` varchar(20) NOT NULL DEFAULT '',
  `tgl_beli` date DEFAULT NULL,
  PRIMARY KEY (`no_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_belanja_bulanan`
--

LOCK TABLES `t_belanja_bulanan` WRITE;
/*!40000 ALTER TABLE `t_belanja_bulanan` DISABLE KEYS */;
INSERT INTO `t_belanja_bulanan` VALUES ('INV-20200707001','2020-07-07'),('INV-20200707002','2020-07-07');
/*!40000 ALTER TABLE `t_belanja_bulanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_beli`
--

DROP TABLE IF EXISTS `t_beli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_beli` (
  `no_beli` varchar(20) NOT NULL,
  `tgl` date NOT NULL,
  `nama_toko` varchar(35) DEFAULT NULL,
  `cash_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_beli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_beli`
--

LOCK TABLES `t_beli` WRITE;
/*!40000 ALTER TABLE `t_beli` DISABLE KEYS */;
INSERT INTO `t_beli` VALUES ('BEL-00001','2020-07-07','',5);
/*!40000 ALTER TABLE `t_beli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_cash`
--

DROP TABLE IF EXISTS `t_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_cash` (
  `cash_no` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` date NOT NULL,
  `amount` int(11) NOT NULL,
  `deskripsi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cash_no`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_cash`
--

LOCK TABLES `t_cash` WRITE;
/*!40000 ALTER TABLE `t_cash` DISABLE KEYS */;
INSERT INTO `t_cash` VALUES (5,'2020-07-07',1000000,'TES');
/*!40000 ALTER TABLE `t_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_det_beli`
--

DROP TABLE IF EXISTS `t_det_beli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_det_beli` (
  `no_beli` varchar(20) NOT NULL,
  `kode_brg` varchar(10) NOT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_det_beli`
--

LOCK TABLES `t_det_beli` WRITE;
/*!40000 ALTER TABLE `t_det_beli` DISABLE KEYS */;
INSERT INTO `t_det_beli` VALUES ('BEL-00001','ITM-001',120,100);
/*!40000 ALTER TABLE `t_det_beli` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `t_det_beli_bef_in` BEFORE INSERT ON `t_det_beli` FOR EACH ROW BEGIN
	INSERT INTO kartu_stok (tgl,in_,last_stok,barang,ket) 
	VALUES(CURDATE(),new.qty,new.qty +(SELECT stok FROM m_brg WHERE kode_brg = new.kode_brg),new.kode_brg,'BELANJA TUNAI');
	UPDATE m_brg SET stok = stok + new.qty WHERE kode_brg = new.kode_brg;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `t_det_invoice`
--

DROP TABLE IF EXISTS `t_det_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_det_invoice` (
  `no_invoice` varchar(20) NOT NULL,
  `kode_brg` varchar(5) NOT NULL,
  `qty` int(5) NOT NULL,
  PRIMARY KEY (`no_invoice`,`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_det_invoice`
--

LOCK TABLES `t_det_invoice` WRITE;
/*!40000 ALTER TABLE `t_det_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_det_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_det_pemesanan`
--

DROP TABLE IF EXISTS `t_det_pemesanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_det_pemesanan` (
  `no_pesan` varchar(15) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  KEY `no_pesan` (`no_pesan`),
  KEY `id_menu` (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_det_pemesanan`
--

LOCK TABLES `t_det_pemesanan` WRITE;
/*!40000 ALTER TABLE `t_det_pemesanan` DISABLE KEYS */;
INSERT INTO `t_det_pemesanan` VALUES ('100001',1,1),('100002',2,1),('100003',1,1),('100004',5,1),('100005',3,1),('100005',2,1),('100006',3,1),('100007',3,1),('100008',2,1),('100009',3,1),('100010',1,1),('100011',1,1),('100012',3,1),('100013',2,1),('100014',6,1),('100016',5,1),('100017',5,1),('100018',6,1),('100018',9,1),('100019',1,1),('100020',1,1),('100021',3,1),('100022',2,1),('100023',2,1),('100024',2,1),('100025',1,1),('100026',1,1),('100027',3,1),('100028',3,1),('100029',2,1),('100030',3,1),('100031',6,1),('100032',2,1),('100033',2,1);
/*!40000 ALTER TABLE `t_det_pemesanan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `det_pemesanan_bef_in` BEFORE INSERT ON `t_det_pemesanan` FOR EACH ROW BEGIN
	INSERT INTO kartu_stok (tgl,out_,last_stok,barang,ket) 

	SELECT CURDATE(),a.qty * d.qty AS total_use,
	e.stok - (a.qty * d.qty) AS last_stok,d.kode_brg,'SALES'
	FROM t_det_pemesanan a 
	INNER JOIN m_menu b ON a.id_menu=b.id_menu
	INNER JOIN m_resep c ON b.id_menu = c.id_menu 
	INNER JOIN t_det_resep d ON c.no_resep = d.no_resep 
	INNER JOIN m_brg e ON d.kode_brg = e.kode_brg WHERE a.id_menu= new.id_menu
	GROUP BY d.kode_brg,a.id_menu ;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `t_det_pesan_af_in` AFTER INSERT ON `t_det_pemesanan` FOR EACH ROW BEGIN
	
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `t_det_po`
--

DROP TABLE IF EXISTS `t_det_po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_det_po` (
  `kode_brg` varchar(5) NOT NULL,
  `no_po` varchar(20) NOT NULL,
  `qty` int(5) NOT NULL,
  PRIMARY KEY (`kode_brg`,`no_po`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_det_po`
--

LOCK TABLES `t_det_po` WRITE;
/*!40000 ALTER TABLE `t_det_po` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_det_po` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_det_resep`
--

DROP TABLE IF EXISTS `t_det_resep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_det_resep` (
  `no_resep` varchar(20) DEFAULT NULL,
  `kode_brg` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_det_resep`
--

LOCK TABLES `t_det_resep` WRITE;
/*!40000 ALTER TABLE `t_det_resep` DISABLE KEYS */;
INSERT INTO `t_det_resep` VALUES ('RSP-001','ITM-001',100),('RSP-001','ITM-002',100),('RSP-002','ITM-004',100),('RSP-002','ITM-005',100),('RSP-002','ITM-006',100),('RSP-001','ITM-003',100),('RSP-003','ITM-008',100),('RSP-003','ITM-009',100),('RSP-003','ITM-010',100);
/*!40000 ALTER TABLE `t_det_resep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_invoice`
--

DROP TABLE IF EXISTS `t_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_invoice` (
  `no_invoice` varchar(20) NOT NULL,
  `tgl` date NOT NULL,
  `no_po` varchar(20) NOT NULL,
  `kd_sup` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `termin` varchar(15) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`no_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_invoice`
--

LOCK TABLES `t_invoice` WRITE;
/*!40000 ALTER TABLE `t_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pembayaran`
--

DROP TABLE IF EXISTS `t_pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pembayaran` (
  `no_bayar` int(11) NOT NULL AUTO_INCREMENT,
  `no_pesan` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `bayar` int(11) DEFAULT NULL,
  `tgl_byr` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` char(1) NOT NULL DEFAULT '0',
  `jenis` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`no_bayar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pembayaran`
--

LOCK TABLES `t_pembayaran` WRITE;
/*!40000 ALTER TABLE `t_pembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pemesanan`
--

DROP TABLE IF EXISTS `t_pemesanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pemesanan` (
  `no_pesan` varchar(15) NOT NULL DEFAULT '',
  `customer` varchar(35) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `tgl_pesan` date NOT NULL,
  `no_meja` varchar(5) DEFAULT NULL,
  `bayar` char(1) DEFAULT '0',
  `amount` int(11) DEFAULT NULL,
  `total_bayar` int(11) DEFAULT '0',
  `kembali` int(11) DEFAULT '0',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `closing` tinyint(1) NOT NULL DEFAULT '0',
  `jenis` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`no_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pemesanan`
--

LOCK TABLES `t_pemesanan` WRITE;
/*!40000 ALTER TABLE `t_pemesanan` DISABLE KEYS */;
INSERT INTO `t_pemesanan` VALUES ('100002','','','2020-07-05','','1',15000,20000,5000,'2020-07-05 15:54:52',6,0,NULL),('100003','','','2020-07-05','','1',15000,20000,5000,'2020-07-05 15:56:16',6,0,NULL),('100004','','','2020-07-06','','1',18000,20000,2000,'2020-07-05 23:07:43',6,0,'CASH'),('100005','','','2020-07-06','','1',33000,0,0,'2020-07-05 23:07:22',6,0,'DEBIT BCA'),('100006','MUHIDIN','087876695069','2020-07-06','1001','1',18000,0,0,'2020-07-06 03:07:17',6,0,'DEBIT MANDIRI'),('100007','','','2020-07-06','','1',18000,50000,32000,'2020-07-06 04:07:55',6,0,'DEBIT BCA'),('100008','','','2020-07-07','','1',15000,50000,35000,'2020-07-07 02:07:50',6,0,'CASH'),('100009','','','2020-07-07','','1',18000,0,0,'2020-07-07 05:07:51',6,0,'OVO'),('100010','','','2020-07-07','','1',15000,20000,5000,'2020-07-06 20:07:22',6,0,'CASH'),('100011','','','2020-07-08','','1',15000,20000,5000,'2020-07-07 22:07:27',6,0,'CASH'),('100012','','','2020-07-08','','1',18000,0,0,'2020-07-07 22:07:35',6,0,'DEBIT BCA'),('100013','',NULL,'2020-07-08','','1',15000,50000,0,'2020-07-08 04:49:56',6,0,'DEBIT MANDIRI'),('100014','',NULL,'2020-07-08','','1',18000,20000,2000,'2020-07-08 04:54:05',6,0,'CASH'),('100015','',NULL,'2020-07-08','','1',18000,50000,32000,'2020-07-08 04:54:46',6,0,'CASH'),('100016','',NULL,'2020-07-08','','1',18000,20000,2000,'2020-07-08 07:29:45',6,0,'CASH'),('100017','',NULL,'2020-07-08','','1',18000,0,0,'2020-07-08 09:03:51',6,0,'DEBIT BCA'),('100018','',NULL,'2020-07-08','','1',36000,50000,14000,'2020-07-08 09:06:04',6,0,'CASH'),('100019','',NULL,'2020-07-08','','1',15000,0,0,'2020-07-08 10:09:38',6,0,''),('100020','','','2020-07-08','','1',15000,50000,35000,'2020-07-08 00:07:10',6,0,'CASH'),('100021','','','2020-07-08','','1',18000,20000,2000,'2020-07-08 01:07:56',6,0,'CASH'),('100022','','','2020-07-08','','1',15000,20000,5000,'2020-07-08 01:07:01',6,0,'CASH'),('100023','','','2020-07-08','','1',15000,0,0,'2020-07-08 01:07:36',6,0,'DEBIT BCA'),('100024','','','2020-07-08','','1',15000,0,0,'2020-07-08 01:07:10',6,0,'DEBIT BCA'),('100025','','','2020-07-08','','1',15000,20000,5000,'2020-07-08 02:07:44',6,0,'CASH'),('100026','','','2020-07-08','','1',15000,20000,5000,'2020-07-08 03:07:21',6,0,'CASH'),('100027','',NULL,'2020-07-08','','1',18000,0,0,'2020-07-08 10:11:09',6,0,''),('100028','',NULL,'2020-07-08','','1',18000,0,0,'2020-07-08 10:13:12',6,0,''),('100029','',NULL,'2020-07-08','','1',15000,0,0,'2020-07-08 10:14:36',6,0,''),('100030','',NULL,'2020-07-08','','0',18000,0,0,'2020-07-08 05:07:41',6,0,NULL),('100031','','','2020-07-08','','1',18000,50000,32000,'2020-07-08 05:07:17',6,0,'CASH'),('100032','','','2020-10-11','','1',15000,50000,35000,'2020-10-10 23:10:57',6,0,'CASH');
/*!40000 ALTER TABLE `t_pemesanan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `update_tmp_det_sales` BEFORE UPDATE ON `t_pemesanan` FOR EACH ROW BEGIN
	UPDATE temp_sales set bayar= new.bayar where no_pesan = OLD.no_pesan;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `t_po`
--

DROP TABLE IF EXISTS `t_po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_po` (
  `no_po` varchar(20) NOT NULL,
  `tgl` date DEFAULT NULL,
  `kd_sup` int(11) NOT NULL,
  PRIMARY KEY (`no_po`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_po`
--

LOCK TABLES `t_po` WRITE;
/*!40000 ALTER TABLE `t_po` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_po` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_resep`
--

DROP TABLE IF EXISTS `t_resep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_resep` (
  `id_resep` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_resep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_resep`
--

LOCK TABLES `t_resep` WRITE;
/*!40000 ALTER TABLE `t_resep` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_resep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_belanja_bulanan`
--

DROP TABLE IF EXISTS `temp_belanja_bulanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_belanja_bulanan` (
  `no_invoice` varchar(20) DEFAULT NULL,
  `kode_brg` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_belanja_bulanan`
--

LOCK TABLES `temp_belanja_bulanan` WRITE;
/*!40000 ALTER TABLE `temp_belanja_bulanan` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_belanja_bulanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_det_beli`
--

DROP TABLE IF EXISTS `temp_det_beli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_det_beli` (
  `no_beli` varchar(10) NOT NULL DEFAULT '',
  `kode_brg` varchar(10) NOT NULL DEFAULT '',
  `harga_beli` int(11) DEFAULT NULL,
  `qty` int(5) DEFAULT NULL,
  PRIMARY KEY (`no_beli`,`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_det_beli`
--

LOCK TABLES `temp_det_beli` WRITE;
/*!40000 ALTER TABLE `temp_det_beli` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_det_beli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_det_pemesanan`
--

DROP TABLE IF EXISTS `temp_det_pemesanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_det_pemesanan` (
  `no_pesan` varchar(15) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  KEY `no_pesan` (`no_pesan`),
  KEY `id_menu` (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_det_pemesanan`
--

LOCK TABLES `temp_det_pemesanan` WRITE;
/*!40000 ALTER TABLE `temp_det_pemesanan` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_det_pemesanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_det_resep`
--

DROP TABLE IF EXISTS `temp_det_resep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_det_resep` (
  `no_resep` varchar(15) DEFAULT NULL,
  `kode_brg` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_det_resep`
--

LOCK TABLES `temp_det_resep` WRITE;
/*!40000 ALTER TABLE `temp_det_resep` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_det_resep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_det_sales`
--

DROP TABLE IF EXISTS `temp_det_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_det_sales` (
  `no_pesan` varchar(15) NOT NULL DEFAULT '',
  `id_menu` int(11) NOT NULL,
  `qty` int(5) DEFAULT NULL,
  PRIMARY KEY (`no_pesan`,`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_det_sales`
--

LOCK TABLES `temp_det_sales` WRITE;
/*!40000 ALTER TABLE `temp_det_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_det_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_sales`
--

DROP TABLE IF EXISTS `temp_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_sales` (
  `no_pesan` varchar(15) NOT NULL DEFAULT '',
  `customer` varchar(35) DEFAULT NULL,
  `tgl_pesan` date NOT NULL,
  `no_meja` varchar(5) DEFAULT NULL,
  `bayar` char(1) DEFAULT '0',
  `total_bayar` int(11) DEFAULT '0',
  `kembali` int(11) DEFAULT '0',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`no_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_sales`
--

LOCK TABLES `temp_sales` WRITE;
/*!40000 ALTER TABLE `temp_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_upload_resep`
--

DROP TABLE IF EXISTS `tmp_upload_resep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_upload_resep` (
  `no_resep` varchar(20) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `kode_brg` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_upload_resep`
--

LOCK TABLES `tmp_upload_resep` WRITE;
/*!40000 ALTER TABLE `tmp_upload_resep` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_upload_resep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `username` varchar(35) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tlp` varchar(13) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-21 12:14:41
