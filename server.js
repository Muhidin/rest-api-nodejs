'use strict';
const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	port = process.env.PORT || 4000;
	
require('dotenv').config()
const cors = require('cors');
var path = require('path');

app.listen(port);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'public')));


// index page
app.get('/tes', function(req, res) {
    res.render('views/barang');
});

console.log('Server started on port: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var routes = require('./routes/appRoutes');
routes(app);