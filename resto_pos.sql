/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.5.20-log : Database - resto_pos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`resto_pos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `resto_pos`;

/*Table structure for table `pos_belanja_harian` */

DROP TABLE IF EXISTS `pos_belanja_harian`;

CREATE TABLE `pos_belanja_harian` (
  `id_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `id_petty` int(11) DEFAULT NULL,
  `tgl_belanja` date DEFAULT NULL,
  PRIMARY KEY (`id_belanja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_brg` */

DROP TABLE IF EXISTS `pos_brg`;

CREATE TABLE `pos_brg` (
  `kode_brg` varchar(15) NOT NULL,
  `nama_brg` varchar(35) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `satuan` varchar(15) DEFAULT NULL,
  `kategori_brg` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_customer` */

DROP TABLE IF EXISTS `pos_customer`;

CREATE TABLE `pos_customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(35) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `pos_det_bel_harian` */

DROP TABLE IF EXISTS `pos_det_bel_harian`;

CREATE TABLE `pos_det_bel_harian` (
  `id_belanja` int(11) NOT NULL,
  `kode_brg` varchar(15) NOT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `qty` int(8) DEFAULT NULL,
  PRIMARY KEY (`id_belanja`,`kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_det_penjualan` */

DROP TABLE IF EXISTS `pos_det_penjualan`;

CREATE TABLE `pos_det_penjualan` (
  `no_penjualan` int(11) NOT NULL,
  `qty` int(8) NOT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`no_penjualan`,`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_kategori_brg` */

DROP TABLE IF EXISTS `pos_kategori_brg`;

CREATE TABLE `pos_kategori_brg` (
  `kategori_brg` varchar(15) NOT NULL,
  PRIMARY KEY (`kategori_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_kategori_menu` */

DROP TABLE IF EXISTS `pos_kategori_menu`;

CREATE TABLE `pos_kategori_menu` (
  `kategori_menu` varchar(25) NOT NULL,
  PRIMARY KEY (`kategori_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_menu` */

DROP TABLE IF EXISTS `pos_menu`;

CREATE TABLE `pos_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(35) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `kategori_menu` varchar(25) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_petty_cash` */

DROP TABLE IF EXISTS `pos_petty_cash`;

CREATE TABLE `pos_petty_cash` (
  `id_petty` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `ket` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_petty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_resep` */

DROP TABLE IF EXISTS `pos_resep`;

CREATE TABLE `pos_resep` (
  `id_resep` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `kode_brg` varchar(15) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  PRIMARY KEY (`id_resep`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pos_suplier` */

DROP TABLE IF EXISTS `pos_suplier`;

CREATE TABLE `pos_suplier` (
  `id_suplier` int(11) NOT NULL AUTO_INCREMENT,
  `nama_suplier` varchar(35) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`id_suplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
