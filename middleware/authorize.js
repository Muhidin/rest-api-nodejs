const jwt = require('jsonwebtoken');
var User = require('../models/userModel.js');

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if(token == null) 
        return res.status(401).send({ message: "Unauthorized", status: false})

    jwt.verify(token, process.env.TOKEN_SCREET, (err, user) => {

        console.log(err);

        if(err) return res.status(403).send({status: false, message: "Access forbidden!"})

        req.user = user
        next()
    })
}

module.exports = authenticateToken;