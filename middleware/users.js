'use strict';

// middleware users.js

module.exports = {
    validateRegister: (req, res, next) => {

        // username min legth 3
        if(req.body.username || req.body.username.length < 3) {
            return res.status(400).send({
                msg: 'Please enter a username with min 3 chars'
            });
        }

        // password min 6 chars
        if(!req.body.password || req.body.password.length < 6) {
            return res.status(400).send({
                msg: 'Please enter a password with min 6 chars'
            });
        }

        // password repeat does not match
        if(!req.body.password_conf || req.body.password != req.body.password_conf) {
            return res.status(400).send({
                msg: 'Password confirmation doesn\'t match '
            });
        }
        next();
    }
}
