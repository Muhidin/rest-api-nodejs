'use strict';

const formidable = require('formidable');
const fs = require('fs');

var ValdoExport = require('../models/valdoExport.js');

// Upload file
exports.uploadData = (req, res) => {

	const form = new formidable.IncomingForm();
	form.parse( req, (err, fields, files)=> {

		const oldpath = files.filetoupload.path;
        const file_ext = files.filetoupload.extname;
		const newpath = '/opt/lampp/htdocs/muamalat_beta/assets/upload/master/'+ files.filetoupload.name;
		console.log(file_ext);
		if(file_ext == '.txt' || file_ext == '.csv') {

			fs.rename(oldpath, newpath, ()=>{
				//if(err) throw err;
				ValdoExport.UploadData(newpath, (err)=> {
					if (err)
						res.send(err);
					res.json({ message: 'Upload file success'});
				})
			})
		} else {
			res.json({ message: 'File failed to upload ! extension not allowed'});
		} 
		
	})


}

 exports.getAll = (req, res) => {
	ValdoExport.getAll( (err,datalist) => {
		// if(err)
		// 	res.status(400).send({
		// 		status: false,
		// 		message: err
		// 	})

		// res.status(200).send({
		// 	// status: true,
		// 	// message:'Success get data barang',
		// 	// data: datalist
		// })
		//console.log(datalist[0]);

		try {

			datalist.forEach(row => {
				ValdoExport.postData(row, () => {
					// if(err)
					// 	res.status(400).send({
					// 		status: false,
					// 		message: err
					// 	})
		
					// res.status(200).send({
					// 	status: true,
					// 	message:'Success post data',
					// })
		
				})
				//console.log(row);
			});
			
		} catch (error) {
			console.log(error);	
		}


	})
}