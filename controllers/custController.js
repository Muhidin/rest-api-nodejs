'use strict';

var Customer = require('../models/custModel.js');

exports.createCust = (req, res) => {

	var new_cust = new Customer(req.body);

	if(!new_cust.nama_customer || !new_cust.no_telp)
	{
		res.status(400).send({ error: true, message: 'Please insert data customer' });
	
	}else {

		Customer.createCustomer(new_cust, (err, customer) => {

			if (err)
				res.send(err);
			res.json(customer);
		});
	}
};


// Get all barang
exports.list_customer = (req, res) => {

	Customer.listCustomer( (err, customer) => {

		console.log('controller');

		if(err)
			res.send(err);
			console.log('res', customer);
		res.send(customer);
	});
};



//get customer by id
exports.getCustById = (req, res) => {

	Customer.getCustomerById (req.params.id, (err, customer) => {

		if (err)
			res.json(err);
		res.json(customer);
	})
};


// Update Customer
exports.updateCustomer = (req, res) => {

	Customer.updateCustomer(req.params.id, new Customer(req.body), (err, customer) => {

		if (err)
			res.send(err);
		res.json(customer);
	})
};

// Delete customer
exports.deleteCustomer = (req, res) => {
	Customer.deleteCustomer(req.params.id, (err, customer) => {

		if (err)
			res.json(err);
		res.json(customer);
	})
};