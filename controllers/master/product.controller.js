'use strict';
const Product = require('../../models/master/product.model');

exports.addProduct = (req, res) => {

    const new_product = new Product(req.body);

    if(!new_product) {
        res.status(400).send({
            error: true,
            message: 'Please provide the field'
        });

    } else {

        // cek product name
        Product.getProductByName(new_product.name, (err,product) => {
            if(err)
                res.status(400).send({
                status: false,
                message: 'Network error!'
            })

            if(product.length > 0) { // jika nama product sudah dipakai
                res.status(200).send({
                    status: false,
                    message: 'Product name have been taken'
                })

            } else { // jika belum
                
                Product.addProduct(new_product, (err, prod) => {

                    if(err)
                        res.status(400).send({
                            status: false,
                            message: err
                        })
                    res.status(200).send({
                        status: true,
                        message: 'Success add product',
                        data: new_product
                    })

                });
            }
        });
    }

}


exports.updateProduct = (req, res) => {

    const new_product = new Product(req.body);
    const product_id = req.params.id

    if(!product_id) {
        res.status(400).send({
            error: true,
            message: 'Please add params productId'
        });

    } else {

        // cek product name
        Product.getProductById(product_id, (err,product) => {
            if(err)
                res.status(400).send({
                status: false,
                message: 'Network error!'
            })

            if(product.length == 0) { // jika product tidak ditemukan
                res.status(200).send({
                    status: false,
                    message: 'Product not found'
                })

            } else { // jika ditemukan
                
                Product.updateProduct(new_product,product_id, (err, prod) => {

                    if(err)
                        res.status(400).send({
                            status: false,
                            message: err
                        })
                    res.status(200).send({
                        status: true,
                        message: 'Success update product',
                        data: new_product
                    })

                });
            }
        });
    }

};

exports.deleteProduct = (req, res) => {

    // const new_product = new Product(req.body);
    const product_id = req.params.id

    if(!product_id) {
        res.status(400).send({
            error: true,
            message: 'Please add params productId'
        });

    } else {

        // cek product name
        Product.getProductById(product_id, (err,product) => {
            if(err)
                res.status(400).send({
                status: false,
                message: 'Network error!'
            })

            if(product.length == 0) { // jika product tidak ditemukan
                res.status(200).send({
                    status: false,
                    message: 'Product not found'
                })

            } else { // jika ditemukan
                
                Product.deleteProduct(product_id, (err, prod) => {

                    if(err)
                        res.status(400).send({
                            status: false,
                            message: err
                        })
                    res.status(200).send({
                        status: true,
                        message: 'Success delete product'
                    })

                });
            }
        });
    }

};


// Get all barang
exports.getAllProduct = (req, res) => {


	Product.getAll( req.query.rowno,req.query.perpage, (err, product) => {

		//console.log('controller');

		if(err)
			res.status(400).send({
				status: false,
				message: err
			})

        Product.totalRow((err, total) => {
            res.status(200).send({
                status: true,
                message:'Success get data',
                data: product,
                total_row: total
            })
        })


		//res.render('/views/barang.ejs')
	});
};

