'use strict';

var Users = require('../models/user.js');

exports.createUser = (req, res) => {

    var new_user = new Users(req.body);

    Users.createUser(new_user, (err, users) => {
        if(err)
            res.send(err);
        res.status(200).send(users);
    })

};