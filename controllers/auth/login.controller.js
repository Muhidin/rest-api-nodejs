'use strict';
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
var User = require('../../models/userModel.js');


exports.login = (req, res) => {
    var new_user = new User(req.body);

    User.cekUser(new_user.username, (err,user) => {

        if(err)
            res.status(400).send({
            status: false,
            message: 'Network error!'
        })


        if(user.length == 0) {
            res.status(404).send({
                status: false,
                message: 'User not found' 
            })

        } else {

            user.forEach(row => {
               let  cek = bcrypt.compareSync(new_user.password, row.password)
               
               //console.log(pass);
                if(cek) {
                    const token = jwt.sign({ sub: row.user_id, username: row.username }, process.env.TOKEN_SCREET , { expiresIn: '1h'});
                    res.status(200).send({
                        status: true,
                        token: token
                    });
                } else {
                    res.status(200).send({
                        status: false,
                        message: 'username or password didn\'t match'
                    })
                }

               
            });


        }


    })

    // let username = new_user.username;
    // let password = hash;

    // console.log(hash);

    // if(username =='' || password=='') {
    //     res.status(400).send({
    //         error: true,
    //         message: 'Please provide username and password'
    //     });

    // }else {
    //     User.login(username,password, (err, user) => {
    //         if(err)
    //             res.status(400).send({
    //                 status: false,
    //                 message: err
    //             })
    //         res.status(200).send({
    //             status: true,
    //             message: "Login success",
    //             token:""
    //         })
    //     });
    // }
};