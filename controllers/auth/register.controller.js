'use strict';
const bcrypt = require('bcryptjs');
const salt = 10;
var User = require('../../models/userModel.js');


exports.registerUser = (req, res) => {

    var new_user = new User(req.body);

    let pass = bcrypt.hashSync(new_user.password,salt)

    var data =[];

    data.push({username:new_user.username,password:pass,fullname:new_user.fullname,email:new_user.email,level:new_user.level});


    if(!data) {
        res.status(400).send({
            error: true,
            message: 'Please provide the field'
        });

    }else{
        User.cekUser(new_user.username, (err,user) => {
            if(err)
                res.status(400).send({
                status: false,
                message: 'Network error!'
            })

            if(user.length > 0) { // jika user sudah dipakai
                res.status(200).send({
                    status: false,
                    message: 'User have been taken'
                })

            } else { // jika belum

                User.register(data, (err, user) => {

                    if(err)
                        res.status(400).send({
                            status: false,
                            message: err
                        })
                    res.status(200).send({
                        status: true,
                        message: 'Success resgister user'
                    })
        
                });

            }

        })
        
   }
};
