'use strict';
// const formidable = require('formidable');
// const fs = require('fs');
// const path = require('path');

var Menu = require('../../models/menu/menu.model.js');

exports.saveMenu = (req, res) => {

    if(req.file) {

        if (ext == 'image/jpg' || ext == 'image/jpeg' || ext== 'image/png') {
            
            let img = 'http://localhost:4000/images/'+req.file.originalname;
            //console.log(img);
            let data = [];
            data.push({item: req.body.item, image: img});
            
            Items.saveItem(data , (err, item) => {

                if(err)
                    res.status(400).send({
                        status: false,
                        message: err
                    })
                res.status(201).send({
                    status: true,
                    message: 'Success save data',
                    data: data
                })
            })
        } else {
            res.status(400).send({
                status: false,
                message: 'Only allowed JPG, JPEG or PNG'
            })
        }

    } else {
        res.status(500).send({
            status: false,
            message: "Internal server error"
        })
    }
};

