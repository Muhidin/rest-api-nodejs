'use strict';

var Config = require('../models/configTokoModel.js');

exports.createToko = (req, res) => {

    var conf = new Config(req.body);

    if(!conf.nama || conf.telp || conf.alamat || conf.email) {
        res.status(400).send({ error: true, message: 'Please complete field'});

    } else {
        Config.createToko(conf, (err, toko) => {

            if(err)
                res.send(err);
            //res.json(toko);
            res.status(200).send({ error: false, message: 'insert success'});
        });
    }
};