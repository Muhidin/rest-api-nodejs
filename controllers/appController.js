'use strict';

const { encodeBase64 } = require('bcryptjs');
const formidable = require('formidable');
const fs = require('fs');

var Barang = require('../models/appModel.js');

exports.createBarang = (req, res) => {

	var new_barang = new Barang(req.body);

	//handle null error
	if(!new_barang.kode_brg || !new_barang.nama_brg) {
		res.status(400).send({ error:true, message: 'Please provide product' });
	
	} else {
		Barang.createBarang(new_barang, (err, barang) => {

			if(err)
			res.status(400).send({
				status: false,
				message: err
			})
			res.status(200).send({
				status: true,
				message:'Success insert data',
				data: new_barang
			})
		});
	}

};

// Get all barang
exports.list_barang = (req, res) => {


	Barang.listBarang( req.query.rowno, (err, barang) => {

		//console.log('controller');

		if(err)
			res.status(400).send({
				status: false,
				message: err
			})

		res.status(200).send({
			status: true,
			message:'Success get data barang',
			data: barang
		})

		//res.render('/views/barang.ejs')
	});
};


// Get user by id
exports.getBrgById = (req, res) => {

	console.log(req.params.id);

	Barang.getBarangById( req.params.id, (err, barang ) => {

		if(err)
			res.status(400).send({
				status: false,
				message: err
			})
		res.status(200).send({
			status: true,
			message:'Success get satuan data barang',
			data: barang
		})
	});
};


// Update user by id
exports.updateBarang = (req, res) => {

	Barang.updateById(req.params.id, new Barang(req.body), (err, barang) => {

		if (err)
			res.send(err);
		res.json(barang);
	})
}


// DELETE users
exports.deleteBarang = (req, res) => {
	Barang.deleteBarang(req.params.id, (err, barang) => {

		if (err)
			res.send(err);
		res.json({ message: 'User successfully deleted' });
	});
};


// Upload file
exports.uploadBarang = (req, res) => {

	const form = new formidable.IncomingForm();
	form.parse( req, (err, fields, files)=> {

		const oldpath = files.filetoupload.path;
		const file_ext = files.filetoupload.extname;
		const newpath = '/opt/lampp/htdocs/muamalat_beta/assets/upload/master/'+ files.filetoupload.name;
		
		if(file_ext == '.csv' || file_ext == '.txt') {

			fs.rename(oldpath, newpath, ()=>{
				//if(err) throw err;
				Barang.uploadBarang(newpath, (err)=> {
					if (err)
						res.send(err);
					res.json({ message: 'Upload file success'});
				})
			})
		} else {
			res.json({ message: 'File failed to upload ! extension not allowed'});
		}
		
	})


}