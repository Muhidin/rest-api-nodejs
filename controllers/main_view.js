'use strict'

const Barang = require('../models/appModel.js');
module.exports = {
    homepage (req, res) {
        res.render('index', { title:'Express' });
    },

    aboutpage (req, res) {
        res.render('about', { title: 'About' });
    },

    listBarang ( req, res) {
        Barang.listBarang( (err, barang) => {
            if(err)
                res.render('error', {title:'Error'});

            res.render('barang_view', {title:'Data Barang', list_barang: barang});
        })
    },

    getBarangById(req, res) {
        Barang.getBarangById( req.params.id, (err, barang) => {
            if(err)
                res.render('error', {title:'Error'});

            res.render('update')
        })
    },

    updateBarang (req, res) {
        Barang.updateById(req.params.id, new Barang(req.body), (err, barang) => {

            if(err)
                res.render('error', {title:'Error'});

            res.render('barang_view', {title:'Data Barang', list_barang: barang});
        })
    },
}