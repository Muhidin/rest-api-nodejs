'use strict';

var Items = require('../../models/list-app/item.js');

exports.listItems = (req, res) => {
    Items.show( (err, item) => {

        if(err)
        res.status(400).send({
            status: false,
            message: err
        })
        res.status(200).send({
            status: true,
            message:'Success get items',
            data: item
        })

    })
}