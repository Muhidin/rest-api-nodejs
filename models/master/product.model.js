'use strict';
const sql = require('../db.js');

const Product = function (product) {
    this.name = product.name;
    this.price = product.price;
    this.stock = product.stock;
    this.unit = product.unit;
    this.category_id = product.category_id;
}

/**
 *  Add product
 */
Product.addProduct = (newProduct, result) => {

    let q = "INSERT INTO `pos_mobile`.`tbl_product` SET ?";
    sql.query(q, newProduct, (err, res) => {

        if(err){
            result(err, null);

        } else {
            result(null, res.insertId);
        }

    });
}

/**
 *  get all product
 */
Product.getAll = (rowno=0,per_page=10, result) => {

    if(rowno != 0) {
		rowno = (rowno-1) * per_page;
	}

    const q = `SELECT * FROM pos_mobile.tbl_product LIMIT ${rowno} , ${per_page}`;
	sql.query(q, (err, res) => {

		if (err) {
			result(null, err);

		}else {
			result(null, res);
		}
	})
}

Product.totalRow = (result) => {

    const q = "SELECT COUNT(*) AS total_row FROM `pos_mobile`.`tbl_product`";
    sql.query(q, (err, res) => {

		if (err) {
			result(null, err);

		}else {
			result(null, res);
		}
	})
}

/*********** MODEL GET  USERS BY ID **********/
Product.getProductById = (id, result) => {
	sql.query("SELECT * FROM `pos_mobile`.`tbl_product` where id =? ", id, (err, res) =>{

		if(err) {
			result(err, null);

		}else {
			result(null, res);
		}
	});
}
/*********** END MODEL GET  USERS BY ID **********/


/**
 *  get product by name
 */
 Product.getProductByName = (name, result) => {
	sql.query("SELECT * FROM `pos_mobile`.`tbl_product` where name =? ", name, (err, res) =>{

		if(err) {
			result(err, null);

		}else {
			result(null, res);
		}
	});
}

/**
 *  Update product
 */
 Product.updateProduct = (newProduct,id, result) => {

    let q = "UPDATE `pos_mobile`.`tbl_product` SET ? WHERE id=?";
    sql.query(q, [newProduct,id], (err, res) => {

        if(err){
            result(err, null);

        } else {
            result(null, res.insertId);
        }

    });
};


/*
* delete product
*/

Product.deleteProduct = (id, result) => {
	sql.query("DELETE FROM `pos_mobile`.`tbl_product` where id =? ", id, (err, res) =>{

		if(err) {
			result(err, null);

		}else {
			result(null, res);
		}
	});
}

module.exports = Product;