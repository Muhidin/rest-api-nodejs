'use strict';
const sql = require('./db.js');

const User = function (user) {
    this.username = user.username;
    this.password = user.password;
    this.fullname = user.fullname;
    this.email = user.email;
    this.level = user.level;
}

/**
 *  create user / register user
 */
User.register = (newUser, result) => {
    sql.query("INSERT INTO `pos_mobile`.`tbl_users` SET ?", newUser, (err, res) => {

        if(err) {
            result(err, null);

        } else {
            result(null, res.insertId);
        }
    });
};

User.cekUser = (username, result) => {
    sql.query("SELECT * FROM `pos_mobile`.`tbl_users` WHERE username=? ", username,(err,res) => {

        if(err) {
            console.log("Error :", err);
            result(err, null);

        }else {
            result(null, res);
        }
    });
};

User.getById = (userid, result) => {
    sql.query("SELECT * FROM `pos_mobile`.`tbl_users` WHERE user_id=? ", userid,(err,res) => {

        if(err) {
            console.log("Error :", err);
            result(err, null);

        }else {
            result(null, res);
        }
    });
};

User.login = (username,password, result) => {
    sql.query("SELECT * FROM `pos_mobile`.`tbl_users` WHERE username=? and password=?", username,password,(err,res) => {

        if(err) {
            console.log("Error :", err);
            result(err, null);

        }else {
            result(null, res);
        }
    });
};

module.exports = User;