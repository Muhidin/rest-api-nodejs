'use strict';

const sql = require('../db.js');

const Items = function(item) {
    this.items = item.items;
}

Items.show = (result) => {

    sql.query("SELECT * FROM `test`.`items`", (err, res) => {

        if(err) {
            console.log("error : ", err);
            result(null, err);
        } else {
            console.log('Items : ', res);

            result(null, res);
        }
    })
};

module.exports = Items;