'use strict';

const sql = require('./db.js');

const Image = function(image) {
    this.type = image.type;
    this.name = image.name;
    this.img = image.img;
}

Image.create = (newImage, result) => {
    sql.query("INSERT INTO test.tbl_image SET ?", newImage, (err, res) => {
        if (err) {
            console.log("Error: ", err)
            result(err, null)

        } else {
            console.log(res.insertId)
            result(null, res.insertId)
        }
    });
};


module.exports = Image;