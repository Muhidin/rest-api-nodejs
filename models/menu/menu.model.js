'use strict';
const sql = require('../db.js');

const Menu = function (menu) {
    this.id_menu = menu.id_menu;
    this.menu = menu.menu;
    this.price = menu.price;
    this.image = menu.image;
    this.kategori = menu.category;
}

Menu.insertData = (newMenu, result) => {
    sql.query("INSERT INTO `pos_mobile`.`tbl_menu` SET ? ", newMenu, (err, res) => {

        if(err) {
            console.log("Error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    })
}


module.exports = Menu;