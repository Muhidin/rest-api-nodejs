'use strict';

const sql = require('./db.js');

const Config = function (cnf) {
    this.nama = cnf.nama;
	this.alamat = cnf.alamat;
	this.email = cnf.email;
	this.telp = cnf.telp;
	this.prefix = cnf.prefix;
	this.status = cnf.status;
	this.exp_date = cnf.exp_date;

}


Config.createToko = (conf, result) => {
	sql.query("INSERT INTO `pos_config`.`con_toko` SET ?", conf, (err, res)=> {

		if(err) {
			console.log("error :", err)
			result(err, null);
		} else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});
};

module.exports = Config;