'use strict';

const sql = require('./db.js');

const ValdoExport = function (valdoexport) {
       this.AgreementNo = valdoexport.AgreementNo; 
       this.FullName = valdoexport.FullName;
       this.OVD = valdoexport.OVD
       this.Bucket = valdoexport.Bucket
       this.Tenor = valdoexport.Tenor
       this.DueDate = valdoexport.DueDate
       this.AngsuranKe = valdoexport.AngsuranKe
       this.OSBalance  = valdoexport.OSBalance
       this.AmountDue = valdoexport.AmountDue
       this.Denda = valdoexport.Denda
       this.Deposit = valdoexport.Deposit
       this.Region = valdoexport.Region
       this.Branch = valdoexport.Branch
       this.POSName = valdoexport.POSName
       this.ResidenceCity = valdoexport.ResidenceCity
       this.ResidenceKecamatan = valdoexport.ResidenceKecamatan
       this.ResidenceKelurahan = valdoexport.ResidenceKelurahan
       this.ResidenceZipCode  = valdoexport.ResidenceZipCode
       this.ResidenceAddress = valdoexport.ResidenceAddress
       this.CompanyPhone  = valdoexport.CompanyPhone
       this.ResidencePhone = valdoexport.ResidencePhone
       this.MobilePhone = valdoexport.MobilePhone
       this.EmergencyPhone = valdoexport.EmergencyPhone
       this.YearSales = valdoexport.YearSales
       this.MonthSales = valdoexport.MonthSales
       this.CMO = valdoexport.CMO
       this.CollectorID = valdoexport.CollectorID
       this.DealerName = valdoexport.DealerName
       this.Category = valdoexport.Category
       this.Product= valdoexport.Product
       this.AOID = valdoexport.AOID
       this.SurveyorId = valdoexport.SurveyorId
       this.CAID = valdoexport.CAID
       this.Brand = valdoexport.Brand
       this.Model = valdoexport.Model
       this.SerialNo1 = valdoexport.SerialNo1
       this.NoPolisi = valdoexport.NoPolisi
       this.Warna = valdoexport.Warna
       this.BirthPlaceDate = valdoexport.BirthPlaceDate
       this.Gender = valdoexport.Gender
       this.MaritalStatus = valdoexport.MaritalStatus
       this.UMUR = valdoexport.UMUR
       this.IbuKandung = valdoexport.IbuKandung
       this.StatusRumah = valdoexport.StatusRumah
       this.Pekerjaan = valdoexport.Pekerjaan
       this.Bagian = valdoexport.Bagian
       this.Jabatan = valdoexport.Jabatan
       this.LamaKerja = valdoexport.LamaKerja
       this.TotalIncome = valdoexport.TotalIncome
       this.EmergencyContactName = valdoexport.EmergencyContactName
       this.EMRCONTACTRELATIONS = valdoexport.EMRCONTACTRELATIONS
       this.LegalAddress= valdoexport.LegalAddress
       this.LegalKelurahan= valdoexport.LegalKelurahan
       this.LegalKecamatan = valdoexport.LegalKecamatan
       this.LegalCity= valdoexport.LegalCity
       this.AssetDescription= valdoexport.AssetDescription
       this.NoTelpBaru = valdoexport.NoTelpBaru
        
};

ValdoExport.UploadData = (file, result) => {
    sql.query("LOAD DATA INFILE '"+file+"' INTO TABLE finansia_beta.ValdoExport FIELDS TERMINATED BY '|'  IGNORE 1 LINES ", (err, res) => {

        if (err) {
            console.log("ERROR :", err);
            result(err, null);
        } else {
            result(null, res);
        }
    })
};

ValdoExport.getAll = (result) => {
    sql.query(`SELECT * FROM finansia_beta.ValdoExport `, (err,res)=> {
        if (err) {
			//console.log("error: ", err);
			result(null, err);

		}else {
			//console.log('Barang : ', res);

			result(null, res);
		}
    })
}

ValdoExport.postData = (data,result) => {
    sql.query(`INSERT IGNORE INTO finansia_collection.ValdoExport SET ? `, data, (err,res) => {
        if(err) {
			console.log("error: ", err);
			result(err, null);

		}else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
    })
}

module.exports = ValdoExport;
