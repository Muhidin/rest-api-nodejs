'use strict';

const sql = require('./db.js');


const Barang = function(barang){
	this.kode_brg = barang.kode_brg;
	this.nama_brg = barang.nama_brg;
	this.harga = barang.harga;
	this.stok = barang.stok;
	this.unit = barang.unit;
	this.kategori = barang.kategori;
};

/*********** MODEL CREATE USER ***********/
Barang.createBarang =  (newBarang, result) => {

	sql.query("INSERT INTO `pos_master`.`m_brg` set ?", newBarang,  (err, res) => {

		if(err) {
			console.log("error: ", err);
			result(err, null);

		}else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});
};
/********* END MODEL CREATE USER **************/



/*********** MODEL GET ALL USERS **********/
Barang.listBarang = (rowno=0,result) => {
	const per_page = 10;
	
	if(rowno != 0) {
		rowno = (rowno-1) * per_page;
	}

	const q = `SELECT * FROM pos_master.m_brg LIMIT ${rowno} , ${per_page}`;
	sql.query(q, (err, res) => {

		if (err) {
			//console.log("error: ", err);
			result(null, err);

		}else {
			//console.log('Barang : ', res);

			result(null, res);
		}
	})

	
};

/*********** END MODEL GET ALL USERS **********/




/*********** MODEL GET  USERS BY ID **********/
Barang.getBarangById = (id, result) => {
	sql.query("SELECT * FROM pos_mobile.tbl_product where id =? ", id, (err, res) =>{

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res);
		}
	});
}
/*********** END MODEL GET  USERS BY ID **********/



/***********  MODEL UPDATE  USERS  **********/
Barang.updateById = (id, barang, result) => {

	sql.query("UPDATE pos_master.m_brg SET nama_brg = ?, harga =? , satuan=?, kategori_brg = ? WHERE kode_brg = ?", [barang.nama_brg,barang.harga,barang.satuan, barang.kategori_brg, id], (err, res)=>{

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res);
		}
	});
};

/***********  END MODEL UPDATE  USERS  **********/



/***********  MODEL DELETE  USERS  **********/
Barang.deleteBarang = (id, result) => {
	sql.query("DELETE FROM pos_master.m_brg where kode_brg = ? ", [id], (err, res) => {

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res);
		}
	})
}
/***********  END MODEL DELETE  USERS  **********/

//$sql = "LOAD DATA INFILE '" . $file . "' INTO TABLE debtor_main_master_inventory
//        FIELDS TERMINATED BY ';' ESCAPED BY '\\\' IGNORE 1 LINES SET create_date=date(now())

Barang.uploadBarang = (file, result) => {
	sql.query("LOAD DATA INFILE '"+file+"' INTO TABLE pos_master.m_brg FIELDS TERMINATED BY ';'  IGNORE 1 LINES (kode_brg,nama_brg,harga,stok,unit,kategori)", (err, res) => {

			if (err) {
				console.log("ERROR :", err);
				result(err, null);
			} else {
				result(null, res);
			}
		})
}

module.exports = Barang;