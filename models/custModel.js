'use strict';

var sql = require('./db.js');

var Customer = function(customer) {
	this.nama_customer = customer.nama_customer;
	this.no_telp = customer.no_telp;
};

Customer.createCustomer = (newCustomer, result) => {

	sql.query("INSERT INTO pos_customer set ?", newCustomer, (err, res)=> {

		if(err) {
			console.log("error: ", err);
			result(err, null);

		}else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});
};



/*********** MODEL GET ALL USERS **********/
Customer.listCustomer = (result) => {

	sql.query("SELECT * FROM pos_customer", (err, res) => {

		if (err) {
			console.log("error: ", err);
			result(null, err);

		}else {
			console.log('Customer : ', res);

			result(null, res);
		}
	})
};

/*********** END MODEL GET ALL USERS **********/

Customer.getCustomerById = (id, result) => {

	sql.query("SELECT * FROM pos_customer WHERE id_customer =?", id, (err, res) => {

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res)
		}
	})
}


/**************** MODEL UPDATE CUSTOMER ***********/

Customer.updateCustomer = (id, customer, result) => {

	sql.query("UPDATE pos_customer SET nama_customer= ?, no_telp= ? WHERE id_customer= ?", [customer.nama_customer, customer.no_telp, id], (err, res) => {

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res);
		}
	})
}

/************ END MODEL UPDATE CUSTOMER ***********/


// Delete customer
Customer.deleteCustomer = (id,  result) => {

	sql.query("DELETE FROM pos_customer WHERE id_customer = ?", [id], (err, res) => {

		if(err) {
			console.log("error :", err);
			result(err, null);

		}else {
			result(null, res);
		}
	})
}


module.exports = Customer;